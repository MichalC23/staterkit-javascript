class Account {
    constructor(number, balance, currency) {
        this.number = number;
        this.balance = balance;
        this.currency = currency;
    }
}

class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    _calculateBalance() {
        let sum = 0;
        for (let account of this.accounts) {
            sum += account.balance;
        }
        return sum;
    };

    findAccount(accountNumber) {
        return this.accounts.find(account => account.number == accountNumber);
    }

    withdraw(accountNumber, amount) {
        return new Promise((resolve, reject) => {
            let account = this.findAccount(accountNumber);
            if (account && amount <= account.balance && amount > 0) {
                setTimeout(() => {
                    account.balance -= amount;
                    resolve({
                        msg: `Account n: ${accountNumber} -  new balance: ${account.balance} ${account.currency}, withdrawn: ${amount} ${account.currency}.`,
                        balance: account.balance
                    });
                }, 3000);
            } else if (!account ) {
                reject(`No account n ${accountNumber}`);
            } else if ( amount > account.balance){
                reject(`Withdrawal amount higher than available balance.`);
            }else if (amount < 0){
                reject(`Cannot withdraw negative amount.`);
            }
        })
    }

    sayHello() {
        return `name:  ${this.firstName}, surname:  ${this.lastName}, accounts number: ${this.accounts.length}, total balance: ${this._calculateBalance()}`
    }

    filterPositiveAccounts() {
        return this.accounts.filter(elem => elem.balance > 0);
    }

    addAccount(account) {
        this.accounts.push(account);
    }
}

const UncleBob = new Person("Robert A.", "Lilly", [new Account(1, 666, "alicks"), new Account(2, 93, "thelems")]);
console.log(UncleBob.sayHello());
UncleBob.addAccount(new Account(3, 23, "fnords"));
console.log(UncleBob.filterPositiveAccounts());
console.log(UncleBob.sayHello());

UncleBob.withdraw(1, 248)
    .then((success) => {
        console.log(success.msg);
    })
    .catch((failure) => {
        console.log(failure)
    });
UncleBob.withdraw(2, 248)
    .then((success) => {
        console.log(success.msg);
    })
    .catch((failure) => {
        console.log(failure)
    });
UncleBob.withdraw(5, 333)
    .then((success) => {
        console.log(success.msg);
    })
    .catch((failure) => {
        console.log(failure)
    });
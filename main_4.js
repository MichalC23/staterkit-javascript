class Account {
    constructor(balance, currency) {
        this.balance = balance;
        this.currency = currency;
    }
}

class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    _calculateBalance() {
        let sum = 0;
        for (let account of this.accounts) {
            sum += account.balance;
        }
        return sum;
    };

    sayHello() {
        return `name:  ${this.firstName}, surname:  ${this.lastName}, accounts number: 
        ${this.accounts.length}, total balance: ${this._calculateBalance()} `
    }

    filterPositiveAccounts() {
        return this.accounts.filter(elem => elem.balance > 0);
    }

    addAccount(account) {
        this.accounts.push(account);
    }
}

const UncleBob = new Person("Robert", "Lilly", [new Account(418, "alicks"), new Account(93, "thelems")]);
console.log(UncleBob.sayHello());
UncleBob.addAccount(new Account(23, "fnords"));
console.log(UncleBob.filterPositiveAccounts());
console.log(UncleBob.sayHello());



let Account = function(balance, currency){
    this.balance = balance;
    this.currency = currency;
};

let person = (function () {
    let details = {
        firstName: "James",
        lastName: "Kirk"
    };

    function calculateBalance() {
      //  person.accounts.map(e => e.balance).reduce((acc, e) => acc + e, 0);
        let accountsSum =0;
        for (let i = 0; i < person.accounts.length; i++) {
            accountsSum += person.accounts[i].balance;
        }
        return accountsSum;
    }

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accounts: [new Account(100,"geld"), new Account(418, "green")],
        sayHello: function () {
            return "name: " + this.firstName + ", surname:  " + this.lastName + ", accounts number: " + this.accounts.length + ", total balance: " + calculateBalance()
        },
        addAccount: function(account){
            this.accounts.push(account);
        }
    }
})();

console.log(person.sayHello());
person.addAccount(new Account(1000, "bucks"));
console.log(person.sayHello());



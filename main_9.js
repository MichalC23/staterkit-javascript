const init = (() => {

    const UncleBob = new Person("Robert A.", "Lilly", [new Account(1, 666, "alicks"), new Account(2, 93, "thelems")]);
    UncleBob.addAccount(new Account(3, 23, "fnords")); //third account, just for fun

    document.addEventListener("DOMContentLoaded", function () {
        const personNameBox = document.querySelector(".card-title");
        const personAccBox = document.querySelector(".card-text");
        const numberInput = document.querySelector("#number");
        const amountInput = document.querySelector("#amount");
        const withdrawButton = document.querySelector(".btn.btn-primary");
        const messageBox = document.querySelector("#message");

        withdrawButton.setAttribute('disabled', 'true');

        function inputChecker() {
            let accNumber = numberInput.value;
            let amount = amountInput.value;

            withdrawButton.disabled = !(accNumber && amount);
        }

        function withdrawal(event, personData) {
            let accNumber = parseInt(numberInput.value);
            let amount = parseFloat(amountInput.value);

            personData.withdraw(accNumber, amount)
                .then((success) => {
                    messageBox.innerText = success.msg;
                    const accSelector = document.querySelector("#acc_" + accNumber);
                    const foundAccount = personData.findAccount(accNumber);
                    accSelector.innerText = `account n: ${foundAccount.number}, amount: ${foundAccount.balance}, currency: ${foundAccount.currency}`
                })
                .catch((failure) => {
                    messageBox.innerText = failure;
                });
        }

        function createPersonCard(personData) {
            personNameBox.innerText = ` ${personData.firstName} ${personData.lastName}`;
            for (let i = 0; i < personData.accounts.length; i++) {
                let number = personData.accounts[i].number;
                let newPara = document.createElement("p");
                newPara.id = "acc_" + number;
                let newParaText = document.createTextNode(`account n: ${personData.accounts[i].number}, amount: ${personData.accounts[i].balance}, currency: ${personData.accounts[i].currency}`);
                newPara.appendChild(newParaText);
                personAccBox.appendChild(newPara);
            }
        }

        withdrawButton.addEventListener("click", (event) => withdrawal(event, UncleBob));
        numberInput.addEventListener("change", inputChecker);
        amountInput.addEventListener("change", inputChecker);
        numberInput.addEventListener("keyup", inputChecker);
        amountInput.addEventListener("keyup", inputChecker);

        createPersonCard(UncleBob);
    });
})();
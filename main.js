let personFactory = function () {
    let details = {
        firstName: "James",
        lastName: "Kirk",
        accounts: [{
            balance: 100000000,
            currency: "credits"
        },
            {
                balance: 418,
                currency: "latinum"
            },
            {
                balance: 10,
                currency: "gold"
            }]

    };
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        sayHello: function () {
            return details.firstName + " " + details.lastName +" " + details.accounts.length
        }
    }
};


let jamesKirk = personFactory();
console.log(jamesKirk.sayHello());
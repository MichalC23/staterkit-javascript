let person = (function () {
    let details = {
        firstName: "James",
        lastName: "Kirk",
        accounts: [{
            balance: 100000000,
            currency: "credits"
        },   {
        balance: 418,
            currency: "latinum"
    },    {
        balance: 10,
            currency: "gold"
    }]
    };
    function calculateBalance() {
        let accountsSum =0;
        for (let i = 0; i < details.accounts.length; i++) { //ew person
            accountsSum += details.accounts[i].balance;    //ew person
        }
        return accountsSum;
    };

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accounts: details.accounts,
        sayHello: function () {
            return "name: " + this.firstName + ", surname:  " + this.lastName + ", accounts number: " + this.accounts.length + ", total balance: " + calculateBalance() //jeśli person to bez call
        },

        addAccount: function(balance, currency){
            this.accounts.push({balance:balance, currency:currency});
        }
    }
})();


console.log(person.sayHello());
person.addAccount(1200,"Romulan ale bottles");
console.log(person.sayHello());